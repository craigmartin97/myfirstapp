package uk.ac.tees.u0018196.myfirstapp;

/**
 * Created by q5031372 on 30/01/18.
 */
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {
    private String storeMessage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.textView);
        textView.setText(message);
        storeMessage = message;
    }



    public void makeCall(View view)
    {
        Uri number = Uri.parse("tel:01642461438");
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);

        if(callIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(callIntent);
        }
    }

    public void launchWebSite(View view)
    {
        Uri webPage = Uri.parse("https://www.google.co.uk");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webPage);
        startActivity(webIntent);
    }

    public void shareText(View view)
    {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, storeMessage);
        sendIntent.setType("text/plain");

        Intent chooser = Intent.createChooser(sendIntent,  "Share with friends");
        if(sendIntent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(chooser);
        }
    }
}

